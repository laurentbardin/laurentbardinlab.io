{
  "$schema": "https://raw.githubusercontent.com/jsonresume/resume-schema/v1.0.0/schema.json",
  "basics": {
    "name": "Laurent Bardin",
    "label": "Développeur",
    "email": "contact@laurentbardin.dev",
    "website": "https://laurentbardin.dev",
    "summary": "Je suis un développeur autodidacte spécialisé dans les technologies du web, avec une préférence pour les développements côté serveur. J'ai travaillé sur de nombreux projets au cours des 15 dernières années, acquérant à cette occasion une bonne expérience sur des sujets connexes tels que le **DevOps**, l'administration de base de données, la gestion **CI/CD**, etc. Récemment, je me suis tourné vers **Python** et **Go** afin d'approfondir ma connaissance de ces deux langages et d'intégrer une équipe les utilisant.",
    "image": "/photo.png",
    "location": {
      "city": "Paris",
      "countryCode": "FR"
    },
    "profiles": [
      {
        "network": "github",
        "username": "laurentbardin",
        "url": "https://github.com/laurentbardin"
      },
      {
        "network": "gitlab",
        "username": "laurentbardin",
        "url": "https://gitlab.com/laurentbardin"
      }
    ]
  },
  "work": [
    {
      "company": "Asmodee Digital / Twin Sails (Asmodee Group)",
      "location": "Paris, FR",
      "description": "Éditeur et producteur de jeux vidéos",
      "position": "Développeur Fullstack",
      "url": "https://account.asmodee.net/",
      "startDate": "2017-02",
      "endDate": "2024-06",
      "keywords": [
          "PHP", "JavaScript", "SQL", "Slim", "Vue",
          "Python", "AWS", "CircleCI", "Jenkins", "redis",
          "git", "Docker", "SSO", "OAuth2", "OpenID Connect"
      ],
      "summary": "Asmodee Digital était la division d'Asmodee chargée de produire et publier sur différentes plateformes les versions numériques de nombreux jeux de plateau. Notre équipe était responsable des sites, des services et de l'infrastructure utilisés par ces jeux et leurs utilisateurs : la plateforme [AsmoConnect](https://account.asmodee.net/), une solution **SSO** bâtie sur les protocoles **OAuth2** et **OpenID Connect**, ainsi qu'une [API REST](https://apidoc.asmodee.net/) testée et documentée.",
      "highlights": [
        "Développement et maintenance d'APIs et d'un site de gestion de compte utilisateur écrits en **PHP** (framework [Slim](https://www.slimframework.com/))",
        "Coopération permanente avec l'équipe DevOps, gagnant un peu d'expérience dans le domaine ([Jenkins](https://www.jenkins.io/), **AWS**)",
        "Maintenance et amélioration de nombreux tests sur divers projets **PHP** à l'aide de [Behat](https://behat.org/) et [PHPUnit](https://phpunit.de/)",
        "Migration **CI/CD** depuis un Jenkins interne vers [CircleCI](https://circleci.com/)",
        "Participation à la conception d'une API pour piloter un site d'organisation de tournois, développée en **Python** grâce à [Django REST framework](https://www.django-rest-framework.org/)",
        "Développement de notre outil interne Studio Manager v2 à l'aide de [Vue](https://vuejs.org/)"
      ]
    },
    {
      "company": "IsCool Entertainment",
      "location": "Paris, FR",
      "description": "Jeux & réseaux sociaux",
      "position": "Développeur Frontend",
      "url": "https://www.iscoolentertainment.com/",
      "startDate": "2011-09",
      "endDate": "2015-04",
      "keywords": [
          "JavaScript", "CoffeeScript", "PHP", "Python", "git", "AWS"
      ],
      "summary": "IsCool Entertainment est un studio de jeux en ligne, produisant divers jeux de cartes et de lettres. J'ai travaillé dans deux équipes en tant que développeur front : d'abord sur un jeu de cartes à collectionner intégré à l'écosystème Facebook, puis un second sur iOS.",
      "highlights": [
        "Réécriture totale du jeu IsCool avec le framework [Spine](https://spine.github.io/)",
        "Développement d'une API **JSON-RPC** standardisée",
        "Développement d'un jeu de carte à collectioner sur mobile à l'aide du framework Titanium",
        "Maintenance et corrections sur la partie serveur ([Symfony](https://symfony.com/) pour **PHP**, [Pyramid](https://trypyramid.com/) pour **Python**)"
      ]
    },
    {
      "company": "Owlient",
      "location": "Paris, FR",
      "description": "Jeux web",
      "position": "Développeur Fullstack",
      "startDate": "2007-01",
      "endDate": "2011-08",
      "keywords": [
          "PHP", "SQL", "JavaScript", "Apache", "subversion", "git", "memcache", "redis"
      ],
      "summary": "Owlient, aujourd'hui un studio Ubisoft, est spécialisé dans les jeux web et mobiles. J'ai pu y travailler sur des projets nombreux et variés, allant du framework interne écrit en PHP et les jeux qui l'utilisaient, jusqu'à des activités de R&D se focalisant sur l'architecture et les performances.",
      "highlights": [
        "Participation au développement de jeux utilisant un framework **PHP** interne",
        "Développement de nouvelles fonctionnalités sur ce même framework et de nombreux outils internes",
        "Recherche et développement sur des sujets variés : performances, communication inter-applications, redis, etc."
      ]
    },
    {
      "company": "Owlient",
      "location": "Paris, FR",
      "description": "Jeux web",
      "position": "Développeur stagiaire",
      "startDate": "2006-10",
      "endDate": "2007-01",
      "keywords": [
          "PHP", "SQL", "JavaScript", "Apache", "subversion"
      ],
      "summary": "Owlient, aujourd'hui un studio Ubisoft, est spécialisé dans les jeux web et mobiles. J'y ai effectué un stage de 3 mois avant d'y être engagé à temps plein.",
      "highlights": [
        "Développement d'une bibliothèqe **PHP** pour créer des graphiques",
        "Intégration **HTML** et **CSS** de la seconde version du site web de l'entreprise"
      ]
    }
  ],
  "volunteer": [],
  "education": [
    {
      "institution": "AFPA Marseille",
      "url": "https://www.afpa.fr",
      "area": "Informatique, Développement",
      "studyType": "Titre de Développeur Informatique - Niveau 2",
      "score": "",
      "courses": [
        "SQL, Design de base de données",
        "Programmation procédurale & orientée objet"
      ]
    },
    {
      "institution": "University de Toulon",
      "url": "https://www.univ-tln.fr/",
      "area": "Sciences",
      "studyType": "DEUG",
      "score": "",
      "courses": [
        "Physique-Chimie",
        "Mathématiques-Informatique"
      ]
    },
    {
      "institution": "Lycée Dumont D'Urville",
      "url": "",
      "area": "Sciences",
      "studyType": "Baccalauréat - Série S Physique-Chimie",
      "score": "",
      "courses": [
        "Physique, Chimie, Mathématiques, Biologie"
      ]
    }
  ],
  "awards": [],
  "publications": [],
  "skills": [
    {
      "name": "Langages",
      "level": "",
      "keywords": [
        "PHP", "Python", "Perl",
        "JavaScript",
        "Go", "C"
      ]
    },
    {
      "name": "Développement Web",
      "level": "",
      "keywords": [
        "HTML", "CSS", "Vue"
      ]
    },
    {
      "name": "Bases de données",
      "level": "",
      "keywords": [
        "MySQL",
        "PostgreSQL",
        "Redis"
      ]
    },
    {
      "name": "Outils",
      "level": "",
      "keywords": [
        "git", "GitHub", "GitLab",
        "Docker",
        "vim",
        "CircleCI"
      ]
    },
    {
      "name": "Systèmes d'exploitation",
      "level": "",
      "keywords": [
        "macOS",
        "Linux",
        "Windows"
      ]
    }
  ],
  "languages": [
    {
      "language": "Français",
      "fluency": "Native speaker",
      "fluencyDisplay": "Langue maternelle"
    },
    {
      "language": "Anglais",
      "fluency": "Advanced",
      "fluencyDisplay": "Avancé"
    }
  ],
  "interests": [
    {
      "name": "Technologies",
      "keywords": [
        "Cryptographie"
      ]
    },
    {
      "name": "Arts, Culture",
      "keywords": [
        "Photographe amateur",
        "Amateur de photographie",
        "Cinéphile"
      ]
    },
    {
      "name": "Lecture",
      "keywords": [
        "Science-Fiction",
        "Fantasy",
        "Bandes dessinées"
      ]
    },
    {
      "name": "Jeux, Loisirs",
      "keywords": [
        "Jeux vidéos",
        "Jeux de plateau"
      ]
    },
    {
      "name": "Sport",
      "keywords": [
        "Wing Tsun"
      ]
    }
  ],
  "references": [],
  "projects": [],
  "meta": {
    "theme": "jsonresume-theme-stackoverflow",
    "lang": "fr",
    "canonical": "https://gitlab.com/laurentbardin/laurentbardin.gitlab.io/-/raw/master/resume.json",
    "version": "v1.0.0",
    "lastModified": "2017-12-24T15:53:00"
  }
}
